package ferzle.util;

/**
 * A node for use in a linked list.
 * 
 * @author Chuck Cusack
 * @version 1.0, March 2013
 */
public class Node<T> {

	public Node<T>	next;
	public T		key;

	public Node() {
		key = null;
		next = null;
	}

	public Node(T key) {
		this.key = key;
		next = null;
	}

	public Node(T key, Node<T> next) {
		this.key = key;
		this.next = next;
	}

	public boolean hasNext() {
		return next != null;
	}

	public Node<T> getNext() {
		return next;
	}

	public void setNext(Node<T> next) {
		this.next = next;
	}

	public void setKey(T key) {
		this.key = key;
	}

	public T getKey() {
		return key;
	}
}
