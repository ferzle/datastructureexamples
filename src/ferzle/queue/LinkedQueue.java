package ferzle.queue;

import java.util.Iterator;

/**
 * A simple Queue implementation based on a linked list
 * 
 * @author Chuck Cusack
 * @version 1.0, February 2008
 */
public class LinkedQueue<T> implements QueueInterface<T> {

	// The data
	// Add the appropriate field(s) here.

	/**
	 * Constructor for objects of class LinkedQueue
	 */
	public LinkedQueue() {
	}

	@Override
	public boolean isEmpty() {
		// TODO Implement me!
		return false;
	}

	@Override
	public boolean isFull() {
		// TODO Implement me!
		return false;
	}

	@Override
	public boolean enqueue(T item) {
		// TODO Implement me!
		return false;
	}

	@Override
	public T dequeue() {
		// TODO Implement me!
		return null;
	}

	@Override
	public T peek() {
		// TODO Implement me!
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Implement me if time permits.
		return null;
	}
}
